import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        totUsers: 0,
        users:{},
        showErr: false,
        showSucc: false,
        successText: '',
        errors: {},
    },
    mutations: {
        incrementUsers: state => {
            state.totUsers++;
        },
        decrementUsers: state => {
            state.totUsers--;
        },
        removeUser: (state, data) =>  {
            for (let i = 0; i < state.users.length; i++) {
                let email = state.users[i]['email'];
                let username = state.users[i]['username'];
                if(email == data.userEmail && username == data.userUsername)
                {
                    state.users.splice(i, 1)
                }
            }
        },
        showSucc: (state, message) =>  {
            state.showErr = false;
            state.showSucc = true;
            state.successText = message;
            setTimeout(() => {state.showSucc = false}, 2000);
        },
        showError: (state, errors) =>  {
            state.showSucc = false;
            state.showErr = true;
            state.errors = errors;
            setTimeout(() => {state.showErr = false}, 2000);
        }
    }
})
