require('./bootstrap');

window.Vue = require('vue');

Vue.component('alert-error-component', require('./components/layout/AlertError.vue').default);
Vue.component('alert-success-component', require('./components/layout/AlertSucc.vue').default);
Vue.component('nav-component', require('./components/layout/Nav.vue').default);
Vue.component('app-component', require('./components/App.vue').default);
Vue.component('home-component', require('./components/views/Home.vue').default);
Vue.component('list-user-component', require('./components/views/ListUser.vue').default);
Vue.component('search-user-component', require('./components/views/SearchUser.vue').default);
Vue.component('search-result-component', require('./components/views/SearchResult.vue').default);
Vue.component('user-component', require('./components/views/User.vue').default);
Vue.component('add-user-component', require('./components/views/AddUser.vue').default);
Vue.component('edit-user-component', require('./components/views/EditUser.vue').default);

import {store} from './store/store.js';
import VueRouter from 'vue-router';
import {routes} from './routes.js';
import Vuelidate from 'vuelidate'

Vue.use(VueRouter);
Vue.use(Vuelidate);

const router = new VueRouter({
    mode: 'history',
    routes: routes,
});

export const url = window.location.origin;

const app = new Vue({
    el: '#app',
    store: store,
    router: router,
    created: function () {
        $(document).ready(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
        });
    }
});
