import App from './components/App.vue';
import AddUser from './components/views/AddUser.vue';
import EditUser from './components/views/EditUser.vue';

export const routes =
[
    {
        path: '/',
        name: 'home',
        component: App
    },
    {
        path: '/agregar-usuario',
        name: 'add-user',
        component: AddUser
    },
    {
        path: '/modificar-usuario/:id',
        name: 'edit-user',
        component: EditUser
    },
    {path: '*', redirect:'/'}
]
