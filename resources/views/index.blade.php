@extends('layouts.main')

@section('content')
    <div id="app">
        <nav-component></nav-component>
        <router-view></router-view>
    </div>
@stop

