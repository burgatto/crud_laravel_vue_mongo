<?php

namespace App\Http\Controllers;

use DB;
use App\User;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getUsers()
    {
        $users = DB::collection('users')->orderBy('nombre', 'asc')->get();

        return response()->json(['users' => $users], 201);
    }

    public function addUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'apellido' => 'required|string',
            'nombre' => 'required|string',
            'email' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors'=>$validator->errors()]);
        }

        $user = new User;

        $user->username = $request->username;
        $user->apellido = $request->apellido;
        $user->nombre = $request->nombre;
        $user->email = $request->email;

        if($user->save())
        {
            return response()->json(['user' => $user,'mensaje' => 'Usuario creado con éxito!'], 200);
        }

        return response()->json([
            'errors' => [
                    'error' => [
                        0 => 'No se pudo cargar el usuario'
                    ]
            ]
        ], 500);
        
    }

    public function getUser($id)
    {
        $user = new User;
        $user = $user->getUserById($id);

        return response()->json(['user' => $user], 200);
    }

    public function updateUser(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|string',
            'apellido' => 'required|string',
            'nombre' => 'required|string',
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => $validator->errors()]);
        }

        $user = new User;
        $user = $user->getUserById($id);

        $user->update([
            'username' => $request->username,
            'apellido' => $request->apellido,
            'nombre' => $request->nombre,
            'email' => $request->email,
            'updated_at' => Carbon::now()
        ]);
        
        return response()->json(['user' => $user, 'mensaje' => 'Usuario modificado con éxito!'], 201);
        
    }

    public function searchUsers(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'nullable|required_without_all:email,nombre,apellido',
            'apellido' => 'nullable|string',
            'nombre' => 'nullable|string',
            'email' => 'nullable|string',
        ]);

        if ($validator->fails()) {
            return response()->json(['errors' => [
                    'error' => [
                        0 => 'Ingrese un criterio de búsqueda'
                    ]
                ]
            ]);
        }

        $user = new User;
        $users = $user->searchUsers($request);

        if(count($users) > 0)
        {
            return response()->json(['users' => $users], 200);
        }

        return response()->json(['errors' => [
                'error' => [
                    0 => 'Ningún resultado encontrado'
                ]
            ]
        ]);
    }

    public function delUser($id)
    {
        $user = new User;
        $user = $user->getUserById($id);

        if(!is_null($user))
        {
            $user->delete();

            return response()->json(['mensaje' => 'Usuario eliminado con éxito!'], 201);
        }

        return response()->json([
            'errors' => [
                'error' => [
                    0 => 'No se pudo eliminar el usuario. Usuario no encontrado'
                ]
            ]
        ]);
    }
}
