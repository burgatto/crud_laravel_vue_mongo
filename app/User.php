<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class User extends Eloquent
{
    protected $connection = 'mongodb';
    protected $collection = 'users';
    protected $keyType = 'string';
    protected $fillable = ['username', 'nombre','apellido', 'email', 'updated_at', 'created_at'];
    protected $attributes = ['username', 'nombre','apellido', 'email', 'updated_at', 'created_at'];
    
    public function getUserById($id)
    {
        $query = $this->setConnection($this->connection)->where('_id', $id)->first();

        return $query;
    }

    public function searchUsers($request)
    {
        return $this->setConnection($this->connection)->where(function($query) use ($request){
            if(!is_null($request->username)){
                $query->where('username', 'like', '%'. $request->username . '%');
            }
            if(!is_null($request->nombre)){
                $query->where('nombre', 'like', '%'. $request->nombre . '%');
            }
            if(!is_null($request->apellido)){
                $query->where('apellido', 'like', '%'. $request->apellido . '%');
            }
            if(!is_null($request->email)){
                $query->where('email', 'like', '%'. $request->email . '%');
            }
        })->orderBy('nombre', 'asc')->get();
        
    }
}
