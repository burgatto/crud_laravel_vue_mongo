<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/list-users', 'UserController@getUsers')->name('get.users');
Route::get('/get-user/{id}', 'UserController@getUser')->name('get.user');
Route::get('/search-users/{username?}/{nombre?}/{apellido?}/{email?}', 'UserController@searchUsers')->name('search.users');
Route::post('/add-user', 'UserController@addUser')->name('post.user');
Route::put('/update-user/{id}', 'UserController@updateUser')->name('put.user');
Route::delete('/delete-user/{id}', 'UserController@delUser')->name('delete.user');
